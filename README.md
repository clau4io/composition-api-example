# Esempio Composition API

Esempio introduttivo sulla nuova Composition API disponibile a partire da Vue 3. 
Lezione 17 della Guida a Vue.js su [Mr. Webmaster.it](https://www.mrwebmaster.it)


## Istruzioni

Dopo essersi assicurati di aver installato Node.js `node --version`, scaricare il repository.

```
git clone https://clau4io@bitbucket.org/clau4io/composition-api-example.git
```

Spostarsi nella nuova cartella ed eseguire il comando `npm install` per installare
tutte le dipendenze.

### Avviare server locale durante la fase di sviluppo

Lanciare il comando `npm run serve` per avviare il server locale.

### Compilare e minificare per la pubblicazione
```
npm run build
```
