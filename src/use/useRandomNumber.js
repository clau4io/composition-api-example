import { ref, computed } from '@vue/composition-api';

export default function (minVal = 0, maxVal = 0, precision = 2) {
  let min = ref(minVal);
  let max = ref(maxVal);
  let randomNumber = ref(0);
  let double = computed(() => randomNumber.value * 2);

  function generateNewRandomNumber() {
    randomNumber.value = 
      +(Math.random() * (max.value - min.value) + min.value).toFixed(precision);
    return randomNumber.value;
  }

  return {
    min,
    max,
    randomNumber,
    double,
    generateNewRandomNumber
  };
}